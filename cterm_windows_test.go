// Copyright 2013, Michael Leimon. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cterm

import (
	"fmt"
)

func ExampleSetColors() {
	SetColors(PINK, GRAY)
	fmt.Printf("Hello world!")
	SetColorsSTD()
	// Output:
	// Hello world!
}

func ExampleSetColorsSTD() {
	SetColors(PINK, GRAY)
	fmt.Printf("Here the colors are set to non-default values")
	SetColorsSTD()
	fmt.Printf("\nThis text uses platform default colors...")
	// Output:
	// Here the colors are set to non-default values
	// This text uses platform default colors...
}

func ExampleNewMessage() {
	msg1 := NewMessage("Text with default colors, ")
	msg2 := NewMessage("Text, fg color specified, ", BLUE)
	msg3 := NewMessage("Text, fg and bg color specified.", DBLUE, WHITE)
	fmt.Println(msg1)
	fmt.Println(msg2)
	fmt.Println(msg3)
	// Output:
	// {7 0 Text with default colors, }
	// {9 0 Text, fg color specified, }
	// {1 15 Text, fg and bg color specified.}
}

func ExampleNewMessageBlock() {
	mb := NewMessageBlock([]Message{
		NewMessage("msg one,"),
		NewMessage(" msg two [BLUE],", BLUE),
		NewMessage(" msg three [RED]", RED)})

	mb.Show()
	fmt.Println()
	// Output:
	// msg one, msg two [BLUE], msg three [RED]
}

func ExampleMessageBlock_Append() {
	var mb MessageBlock

	mb.Append("Text with default colors\n")
	mb.Append("Text, fg color specified\n", RED)
	mb.Append("Text, fg and bg color specified\n", DBLUE, WHITE)
	fmt.Println(mb.Blocks[0])
	fmt.Println(mb.Blocks[1])
	fmt.Println(mb.Blocks[2])
	// Output:
	// {7 0 Text with default colors
	// }
	// {12 0 Text, fg color specified
	// }
	// {1 15 Text, fg and bg color specified
	// }
}

func ExampleMessageBlock_Show() {
	var mb MessageBlock

	mb.Append("Text with default colors\n")

	mb.Show()
	// Output:
	// Text with default colors
}
