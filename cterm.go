// Copyright 2013, Michael Leimon. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

// cterm
//
// ColorTerm is a library designed to make cross platform colored console output
// simple.
package cterm

import (
	"fmt"
)

type Color int

const (
	// The following colors can be used to specify text or msg background color.
	BLACK   Color = iota
	DBLUE   Color = iota
	DGREEN  Color = iota
	DCYAN   Color = iota
	DRED    Color = iota
	DPINK   Color = iota
	DYELLOW Color = iota
	GRAY    Color = iota
	DGRAY   Color = iota
	BLUE    Color = iota
	GREEN   Color = iota
	CYAN    Color = iota
	RED     Color = iota
	PINK    Color = iota
	YELLOW  Color = iota
	WHITE   Color = iota
)

// This is the fundamental structure of this library. Each cterm.Message
// contains a string as well as the color information of both the background and
// foreground.
//
// The implementation of the associated method Show() is platform specific.
type Message struct {
	FGcolor Color
	BGcolor Color
	Data    string
}

// The Show method prints out the contained message. With this method, the color
// configuration of the console is always set and then set back to platform
// console defaults.
func (self *Message) Show() {

	if self.FGcolor == STDFG && self.BGcolor == STDBG {
		// The message is uses standard platform colors
		SetColorsSTD()
		fmt.Printf("%s", self.Data)
	} else {
		// A color config other than standard platform colors was specified
		SetColors(self.FGcolor, self.BGcolor)
		fmt.Printf("%s", self.Data)
		SetColorsSTD()
	}
}

// Creates a new (potentailly colorful) message using a minimal amount of input.
//
// At minimum, a message can be created by only specifying a the string contents
// of the Message. When this is done, the default foreground/text color and
// default background color will be used for the message.
//
// Optionally, the forground/text color and the background color of a Message
// can be specified by supplying addtional arguments. The first addtional
// argument (if supplied) will be used as the forground/text color. Similarly,
// the second addtional argument (if supplied) will be used as the background
// color. If it is desired to only set the background color, then the color
// `cterm.STDFG` should be used for the first addtional argument.
func NewMessage(msg string, optColors ...Color) Message {
	fgColor := STDFG
	bgColor := STDBG
	if len(optColors) > 0 {
		fgColor = optColors[0]
	}
	if len(optColors) > 1 {
		bgColor = optColors[1]
	}

	return Message{fgColor, bgColor, msg}
}

// This struct is simply an array of Messages. Using a message block might be
// preferable when constructing a complex set of messages. This struct also has
// the added advantage of providing a single `Show()` method which will print
// out all of the contained messages in the order they appear in the array.
type MessageBlock struct {
	Blocks []Message
}

func NewMessageBlock(msgs []Message) MessageBlock {
	return MessageBlock{msgs}
}

// Appends a new (potentailly colorful) message to the end of a MessageBlock
// using a minimal amount of input.
//
// At minimum, a message can be created by only specifying a the string contents
// of the Message. When this is done, the default foreground/text color and
// default background color will be used for the message.
//
// Optionally, the forground/text color and the background color of a Message
// can be specified by supplying addtional arguments. The first addtional
// argument (if supplied) will be used as the forground/text color. Similarly,
// the second addtional argument (if supplied) will be used as the background
// color. If it is desired to only set the background color, then the color
// `cterm.STDFG` should be used for the first addtional argument.
func (self *MessageBlock) Append(msg string, optColors ...Color) {
	fgColor := STDFG
	bgColor := STDBG
	if len(optColors) > 0 {
		fgColor = optColors[0]
	}
	if len(optColors) > 1 {
		bgColor = optColors[1]
	}

	self.Blocks = append(self.Blocks, Message{fgColor, bgColor, msg})
}

// Appends one message block to the end of another
func (self *MessageBlock) AppendBlock(mb MessageBlock) {
	for _, msg := range mb.Blocks {
		self.Blocks = append(self.Blocks, msg)
	}
}

// The Show method prints out all of the contained messages of a MessageBlock in
// the order they appear in the internal array.
func (self *MessageBlock) Show() {
	var oldColors, theseColors [2]Color
	for _, msg := range self.Blocks {
		theseColors = [2]Color{msg.FGcolor, msg.BGcolor}
		if theseColors != oldColors {
			// Change console colors
			SetColors(msg.FGcolor, msg.BGcolor)
			oldColors = theseColors
		}
		// msg.Show()
		fmt.Printf("%s", msg.Data)
	}
	if oldColors != [2]Color{STDFG, STDBG} {
		SetColorsSTD()
	}
}
