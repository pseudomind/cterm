cterm
=====
ColorTerm (AKA: cterm) is a simple cross-platform go library for generating color terminal output.

Currently it has been designed and tested to work under: Linux, OSX and Windows.

If there is any interest, it could probably be ported to other operating systems without too much effort. 

### Documentation

Online documentation is available at: [http://godoc.org/bitbucket.org/pseudomind/cterm](http://godoc.org/bitbucket.org/pseudomind/cterm)

### Screenshots

![cterm example on OSX](https://bitbucket.org/pseudomind/cterm/raw/689483db89758ac280983958928fce8b28b37e6f/example/example-osx.png)

The provided cterm example on OSX

![cterm example on Windows](https://bitbucket.org/pseudomind/cterm/raw/55a755d423af3b4c6c7c880dafdfe2e650a49042/example/example-windows.PNG)

The provided cterm example on Windows
