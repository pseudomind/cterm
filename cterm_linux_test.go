// Copyright 2013, Michael Leimon. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cterm

import (
	"fmt"
)

func ExampleSetColors() {
	SetColors(PINK, GRAY)
	fmt.Printf("Hello world!")
	SetColorsSTD()
	// Output:
	// [47m[95mHello world![0m
}

func ExampleSetColorsSTD() {
	SetColors(PINK, GRAY)
	fmt.Printf("Here the colors are set to non-default values")
	SetColorsSTD()
	fmt.Printf("\nThis text uses platform default colors...")
	// Output:
	// [47m[95mHere the colors are set to non-default values[0m
	// This text uses platform default colors...
}

func ExampleNewMessage() {
	msg1 := NewMessage("Text with default colors, ")
	msg2 := NewMessage("Text, fg color specified, ", BLUE)
	msg3 := NewMessage("Text, fg and bg color specified.", DBLUE, WHITE)
	fmt.Println(msg1)
	fmt.Println(msg2)
	fmt.Println(msg3)
	// Output:
	// {7 -1 Text with default colors, }
	// {9 -1 Text, fg color specified, }
	// {1 15 Text, fg and bg color specified.}
}

func ExampleNewMessageBlock() {
	mb := NewMessageBlock([]Message{
		NewMessage("msg one,"),
		NewMessage(" msg two [BLUE],", BLUE),
		NewMessage(" msg three [RED]", RED)})

	mb.Show()
	fmt.Println()
	// Output:
	// [0mmsg one,[40m[94m msg two [BLUE],[40m[91m msg three [RED][0m
}

func ExampleMessageBlock_Append() {
	var mb MessageBlock

	mb.Append("Text with default colors\n")
	mb.Append("Text, fg color specified\n", RED)
	mb.Append("Text, fg and bg color specified\n", DBLUE, WHITE)
	fmt.Println(mb.Blocks[0])
	fmt.Println(mb.Blocks[1])
	fmt.Println(mb.Blocks[2])
	// Output:
	// {7 -1 Text with default colors
	// }
	// {12 -1 Text, fg color specified
	// }
	// {1 15 Text, fg and bg color specified
	// }
}

func ExampleMessageBlock_Show() {
	var mb MessageBlock

	mb.Append("Text with default colors\n")

	mb.Show()
	// Output:
	// [0mText with default colors
}
