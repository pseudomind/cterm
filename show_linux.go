// Copyright 2013, Michael Leimon. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cterm

import (
	"fmt"
)

const (
	cBLACK   = 0
	cDBLUE   = 4
	cDGREEN  = 2
	cDCYAN   = 6
	cDRED    = 1
	cDPINK   = 5
	cDYELLOW = 3
	cGRAY    = 7
	cDGRAY   = 1000
	cBLUE    = 1004
	cGREEN   = 1002
	cCYAN    = 1006
	cRED     = 1001
	cPINK    = 1005
	cYELLOW  = 1003
	cWHITE   = 1007
	cTRANSP  = -1
)

const (
	// The following standard colors are specific to the operating platform.
	STDBG Color = (Color)(cTRANSP)
	STDFG Color = GRAY
)

// SetColorsSTD sets the the console foreground and background colors to the
// platform default values.
func SetColorsSTD() { fmt.Printf("\033[0m") }

// SetColors sets the console foreground and background colors to the specified
// values.
func SetColors(fg, bg Color) {
	var prefix, toNorm string
	var colorCodeBG, colorCodeFG int

	toNorm = "\033[0m"

	if fg == STDFG && bg == STDBG {
		// standard foreground standard background colors
		prefix = toNorm
	} else {
		// convert general cterm colors to platform specific integer values
		switch fg {
		case BLACK:
			colorCodeFG = cBLACK
		case DBLUE:
			colorCodeFG = cDBLUE
		case DGREEN:
			colorCodeFG = cDGREEN
		case DCYAN:
			colorCodeFG = cDCYAN
		case DRED:
			colorCodeFG = cDRED
		case DPINK:
			colorCodeFG = cDPINK
		case DYELLOW:
			colorCodeFG = cDYELLOW
		case GRAY:
			colorCodeFG = cGRAY
		case DGRAY:
			colorCodeFG = cDGRAY
		case BLUE:
			colorCodeFG = cBLUE
		case GREEN:
			colorCodeFG = cGREEN
		case CYAN:
			colorCodeFG = cCYAN
		case RED:
			colorCodeFG = cRED
		case PINK:
			colorCodeFG = cPINK
		case YELLOW:
			colorCodeFG = cYELLOW
		case WHITE:
			colorCodeFG = cWHITE
		}

		switch bg {
		case BLACK:
			colorCodeBG = cBLACK
		case DBLUE:
			colorCodeBG = cDBLUE
		case DGREEN:
			colorCodeBG = cDGREEN
		case DCYAN:
			colorCodeBG = cDCYAN
		case DRED:
			colorCodeBG = cDRED
		case DPINK:
			colorCodeBG = cDPINK
		case DYELLOW:
			colorCodeBG = cDYELLOW
		case GRAY:
			colorCodeBG = cGRAY
		case DGRAY:
			colorCodeBG = cDGRAY
		case BLUE:
			colorCodeBG = cBLUE
		case GREEN:
			colorCodeBG = cGREEN
		case CYAN:
			colorCodeBG = cCYAN
		case RED:
			colorCodeBG = cRED
		case PINK:
			colorCodeBG = cPINK
		case YELLOW:
			colorCodeBG = cYELLOW
		case WHITE:
			colorCodeBG = cWHITE
		}

		if colorCodeBG > 999 {
			// a bright color
			colorCodeBG -= 1000
			prefix = fmt.Sprintf("\033[10%dm", colorCodeBG)
		} else {
			// a dark color
			prefix = fmt.Sprintf("\033[4%dm", colorCodeBG)
		}

		if colorCodeFG > 999 {
			// a bright color
			colorCodeFG -= 1000
			prefix = fmt.Sprintf("%v\033[9%dm", prefix, colorCodeFG)
		} else {
			// a dark color
			prefix = fmt.Sprintf("%v\033[3%dm", prefix, colorCodeFG)
		}
	}

	fmt.Printf("%s", prefix)
}
