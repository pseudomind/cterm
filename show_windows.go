// Copyright 2013, Michael Leimon. All rights reserved.
// Use of this source code is governed by a BSD-style
// license that can be found in the LICENSE file.

package cterm

/*
#include <stdio.h>
#include <windows.h>
*/
import "C"

var handle C.HANDLE

const (
	cBLACK   = iota
	cDBLUE   = iota
	cDGREEN  = iota
	cDCYAN   = iota
	cDRED    = iota
	cDPINK   = iota
	cDYELLOW = iota
	cGRAY    = iota
	cDGRAY   = iota
	cBLUE    = iota
	cGREEN   = iota
	cCYAN    = iota
	cRED     = iota
	cPINK    = iota
	cYELLOW  = iota
	cWHITE   = iota
)

const (
	// The following standard colors are specific to the operating platform.
	STDBG Color = BLACK
	STDFG Color = GRAY
)

func init() {
	handle = C.GetStdHandle(C.STD_OUTPUT_HANDLE)
}

// SetColorsSTD sets the the console foreground and background colors to the
// platform default values.
func SetColorsSTD() { C.SetConsoleTextAttribute(handle, (C.WORD)(1*cGRAY + 16*cBLACK) ) }

// SetColors sets the console foreground and background colors to the specified
// values.
func SetColors(fg, bg Color) {
	var colorCodeFG, colorCodeBG int
	var colorCode C.WORD

	// convert general cterm colors to platform specific integer values
	switch fg {
	case BLACK:
		colorCodeFG = cBLACK
	case DBLUE:
		colorCodeFG = cDBLUE
	case DGREEN:
		colorCodeFG = cDGREEN
	case DCYAN:
		colorCodeFG = cDCYAN
	case DRED:
		colorCodeFG = cDRED
	case DPINK:
		colorCodeFG = cDPINK
	case DYELLOW:
		colorCodeFG = cDYELLOW
	case GRAY:
		colorCodeFG = cGRAY
	case DGRAY:
		colorCodeFG = cDGRAY
	case BLUE:
		colorCodeFG = cBLUE
	case GREEN:
		colorCodeFG = cGREEN
	case CYAN:
		colorCodeFG = cCYAN
	case RED:
		colorCodeFG = cRED
	case PINK:
		colorCodeFG = cPINK
	case YELLOW:
		colorCodeFG = cYELLOW
	case WHITE:
		colorCodeFG = cWHITE
	}

	switch bg {
	case BLACK:
		colorCodeBG = cBLACK
	case DBLUE:
		colorCodeBG = cDBLUE
	case DGREEN:
		colorCodeBG = cDGREEN
	case DCYAN:
		colorCodeBG = cDCYAN
	case DRED:
		colorCodeBG = cDRED
	case DPINK:
		colorCodeBG = cDPINK
	case DYELLOW:
		colorCodeBG = cDYELLOW
	case GRAY:
		colorCodeBG = cGRAY
	case DGRAY:
		colorCodeBG = cDGRAY
	case BLUE:
		colorCodeBG = cBLUE
	case GREEN:
		colorCodeBG = cGREEN
	case CYAN:
		colorCodeBG = cCYAN
	case RED:
		colorCodeBG = cRED
	case PINK:
		colorCodeBG = cPINK
	case YELLOW:
		colorCodeBG = cYELLOW
	case WHITE:
		colorCodeBG = cWHITE
	}

	colorCode = (C.WORD)(1*colorCodeFG + 16*colorCodeBG)
	C.SetConsoleTextAttribute(handle, colorCode)
}