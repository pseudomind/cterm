// This sample program demonstrates the capabilites of the cterm library.
package main

import (
	"bitbucket.org/pseudomind/cterm"
	"fmt"
)

func main() {

	cBlkA := cterm.NewMessageBlock([]cterm.Message{
		// line - 1
		cterm.NewMessage("BLACK  ", cterm.BLACK),
		cterm.NewMessage(" "),
		cterm.NewMessage("DBLUE  ", cterm.DBLUE),
		cterm.NewMessage(" "),
		cterm.NewMessage("DGREEN ", cterm.DGREEN),
		cterm.NewMessage(" "),
		cterm.NewMessage("DCYAN  ", cterm.DCYAN),
		cterm.NewMessage(" "),
		cterm.NewMessage("DRED   ", cterm.DRED),
		cterm.NewMessage(" "),
		cterm.NewMessage("DPINK  ", cterm.DPINK),
		cterm.NewMessage(" "),
		cterm.NewMessage("DYELLOW", cterm.DYELLOW),
		cterm.NewMessage(" "),
		cterm.NewMessage("GRAY   ", cterm.GRAY),
		cterm.NewMessage("\n"),
		// line - 2
		cterm.NewMessage("DGRAY  ", cterm.DGRAY),
		cterm.NewMessage(" "),
		cterm.NewMessage("BLUE   ", cterm.BLUE),
		cterm.NewMessage(" "),
		cterm.NewMessage("GREEN  ", cterm.GREEN),
		cterm.NewMessage(" "),
		cterm.NewMessage("CYAN   ", cterm.CYAN),
		cterm.NewMessage(" "),
		cterm.NewMessage("RED    ", cterm.RED),
		cterm.NewMessage(" "),
		cterm.NewMessage("PINK   ", cterm.PINK),
		cterm.NewMessage(" "),
		cterm.NewMessage("YELLOW ", cterm.YELLOW),
		cterm.NewMessage(" "),
		cterm.NewMessage("WHITE  ", cterm.WHITE),
		cterm.NewMessage(" ")})

	cBlkA.Show()

	print("\n\n")

	cList := []cterm.Color{
		cterm.BLACK,
		cterm.DBLUE,
		cterm.DGREEN,
		cterm.DCYAN,
		cterm.DRED,
		cterm.DPINK,
		cterm.DYELLOW,
		cterm.GRAY,
		cterm.DGRAY,
		cterm.BLUE,
		cterm.GREEN,
		cterm.CYAN,
		cterm.RED,
		cterm.PINK,
		cterm.YELLOW,
		cterm.WHITE}

	var cBlk cterm.MessageBlock
	for _, bgColor := range cList {
		for _, fgColor := range cList {
			cBlk.Append(fmt.Sprintf("%0.2dx%0.2d", fgColor, bgColor), fgColor, bgColor)
			cBlk.Append(" ")
		}
		cBlk.Append("\n")
	}
	cBlk.Show()
}
